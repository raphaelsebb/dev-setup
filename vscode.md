# Extensions

- aaron-bond.better-comments
- christian-kohler.path-intellisense
- dbaeumer.vscode-eslint
- devcycles.contextive
- eamodio.gitlens
- EditorConfig.EditorConfig
- esbenp.prettier-vscode
- formulahendry.auto-close-tag
- formulahendry.auto-complete-tag
- formulahendry.auto-rename-tag
- formulahendry.code-runner
- GitHub.copilot
- GitHub.copilot-chat
- jock.svg
- johnpapa.vscode-peacock
- mikestead.dotenv
- ms-azuretools.vscode-docker
- ms-vscode-remote.remote-containers
- ms-vscode.atom-keybindings
- mtxr.sqltools
- naumovs.color-highlight
- PKief.material-icon-theme
- Prisma.prisma
- RandomFractalsInc.vscode-data-preview
- redhat.vscode-yaml
- streetsidesoftware.code-spell-checker
- Tomi.xasnippets
- tommasov.hosts
- usernamehw.errorlens
- vincaslt.highlight-matching-tag
- wmaurer.change-case
- xabikos.JavaScriptSnippets
- yoavbls.pretty-ts-errors
- zhuangtongfa.material-theme
- zhucy.project-tree
