# Visual Studio Code Extentions

## Shortcuts

- [mostafa.change-case](https://marketplace.visualstudio.com/items?itemName=mostafa.change-case)
- [ms-vscode.atom-keybindings](https://marketplace.visualstudio.com/items?itemName=ms-vscode.atom-keybindings)

## Theme

- [johnpapa.vscode-peacock](https://marketplace.visualstudio.com/items?itemName=johnpapa.vscode-peacock)
- [zhuangtongfa.material-theme](https://marketplace.visualstudio.com/items?itemName=zhuangtongfa.material-theme)
- [PKief.material-icon-theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)

## Highlighter

- [aaron-better-comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)
- [CoenraadS.bracket-pair-colorizer-2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
- [eamodio.gitlens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [usernamehw.errorlens](https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens)
- [vincaslt.highlight-matching-tag](https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag)
- [wayou.vscode-todo-highlight](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight)

## Languages

- [abusaidm.html-snippets](https://marketplace.visualstudio.com/items?itemName=abusaidm.html-snippets)
- [burkeholland.simple-react-snippets](https://marketplace.visualstudio.com/items?itemName=burkeholland.simple-react-snippets)
- [christian-kohler.path-intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
- [DavidAnson.vscode-markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- [dsznajder.es7-react-js-snippets](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)
- [formulahendry.auto-close-tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
- [formulahendry.auto-complete-tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-complete-tag)
- [formulahendry.auto-rename-tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [jcbuisson.vue](https://marketplace.visualstudio.com/items?itemName=jcbuisson.vue)
- [mikestead.dotenv](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv)
- [ms-azuretools.vscode-docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
- [octref.vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
- [redhat.vscode-yaml](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml)
- [sdras.vue-vscode-snippets](https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets)
- [xabikos.JavaScriptSnippets](https://marketplace.visualstudio.com/items?itemName=xabikos.JavaScriptSnippets)
- [xabikos.ReactSnippets](https://marketplace.visualstudio.com/items?itemName=xabikos.ReactSnippets)
- [yzhang.markdown-all-in-one](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)

## Linter

- [dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [esbenp.prettier-vscode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [HookyQR.beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify)
- [sibiraj-s.vscode-scss-formatter](https://marketplace.visualstudio.com/items?itemName=sibiraj-s.vscode-scss-formatter)
- [tombonnike.vscode-status-bar-format-toggle](https://marketplace.visualstudio.com/items?itemName=tombonnike.vscode-status-bar-format-toggle)

## Code

- [formulahendry.code-runner](https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner)

## Config

- [IronGeek.vscode-env](https://marketplace.visualstudio.com/items?itemName=IronGeek.vscode-env)
- [EditorConfig.EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)

## Tools

- [MariusAlchimavicius.json-to-ts](https://marketplace.visualstudio.com/items?itemName=MariusAlchimavicius.json-to-ts)
- [rangav.vscode-thunder-client](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client)

// 29/04/2024

atom-keybindings => ms-vscode.atom-keybindings
auto-complete-tag => formulahendry.auto-complete-tag
auto-rename-tag => formulahendry.auto-rename-tag
better-comments => aaron-bond.better-comments
change-case => wmaurer.change-case
code-runner => formulahendry.code-runner
code-spell-checker => streetsidesoftware.code-spell-checker
color-highlight => naumovs.color-highlight
copilot => github.copilot
copilot-chat => github.copilot-chat
dotenv => mikestead.dotenv
editorconfig => editorconfig.editorconfig
errorlens => usernamehw.errorlens
githistory => donjayamanne.githistory
gitlens => eamodio.gitlens
hosts => tommasov.hosts
javascriptsnippets => xabikos.javascriptsnippets
js-auto-backticks => chamboug.js-auto-backticks
material-icon-theme => pkief.material-icon-theme
material-theme => zhuangtongfa.material-theme
multi-cursor-case-preserve => cardinal90.multi-cursor-case-preserve
npm-intellisense => christian-kohler.npm-intellisense
path-intellisense => christian-kohler.path-intellisense
prettier-vscode => esbenp.prettier-vscode
pretty-ts-errors => yoavbls.pretty-ts-errors
prisma => prisma.prisma
python => ms-python.python
react-vscode-extension-pack => jawandarajbir.react-vscode-extension-pack
reactsnippets => xabikos.reactsnippets
remote-containers => ms-vscode-remote.remote-containers
sass-indented => syler.sass-indented
search-node-modules => jasonnutter.search-node-modules
sqltools => mtxr.sqltools
svg => jock.svg
vetur => octref.vetur
vs-code-prettier-eslint => rvest.vs-code-prettier-eslint
vscode-docker => ms-azuretools.vscode-docker
vscode-env => irongeek.vscode-env
vscode-eslint => dbaeumer.vscode-eslint
vscode-kubernetes-tools => ms-kubernetes-tools.vscode-kubernetes-tools
vscode-language-pack-fr => ms-ceintl.vscode-language-pack-fr
vscode-markdownlint => davidanson.vscode-markdownlint
vscode-mdx => unifiedjs.vscode-mdx
vscode-mjml => attilabuti.vscode-mjml
vscode-peacock => johnpapa.vscode-peacock
vscode-pylance => ms-python.vscode-pylance
vscode-sqlite => alexcvzz.vscode-sqlite
vscode-status-bar-format-toggle => tombonnike.vscode-status-bar-format-toggle
vscode-theme-onelight => akamud.vscode-theme-onelight
vscode-todo-highlight => wayou.vscode-todo-highlight
vscode-yaml => redhat.vscode-yaml
vscodeintellicode => visualstudioexptteam.vscodeintellicode
vue-snippets => hollowtree.vue-snippets
vue-vscode-snippets => sdras.vue-vscode-snippets
vuejs-extension-pack => mubaidr.vuejs-extension-pack
vuex-suggest => mishannn.vuex-suggest
xasnippets => tomi.xasnippets