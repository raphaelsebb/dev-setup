# Development setup

## Git

- Add some git alias

```bash
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
git config --global alias.last 'log -1 HEAD'
git config --global alias.ss stash
git config --global alias.ssa 'stash apply'

# One liner
git config --global alias.co checkout && git config --global alias.br branch && git config --global alias.ci commit && git config --global alias.st status && git config --global alias.last 'log -1 HEAD' && git config --global alias.ss stash && git config --global alias.ssa 'stash apply'
```
