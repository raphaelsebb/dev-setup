# Applications

## Browser

- [Arc](https://arc.net/)

### Chrome extensions

- [daily.dev](https://daily.dev/)
- [Linkedin](https://chrome.google.com/webstore/detail/linkedin-extension/meajfmicibjppdgbjfkpdikfjcflabpk)
- [Black Menu for Google](https://apps.jeurissen.co/black-menu-for-google)
- [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

## Password manager

- [Dashlane](https://www.dashlane.com/)
- [1Password](https://1password.com/)

## Terminal

- [warp terminal](https://www.warp.dev/)
- [warp theme](https://github.com/warpdotdev/themes) - `standard/apple_dark.yaml`
- [zsh](https://ohmyz.sh/)
- [brew](https://brew.sh/)
- [nvm](https://github.com/nvm-sh/nvm?tab=readme-ov-file#installing-and-updating)
- pcd functions

## Docker

- [Docker](https://www.docker.com/)

## Database manager

- [Beekeeper Studio](https://www.beekeeperstudio.io/)
- [Metabase](https://www.metabase.com/)

## IDE

- Download [VS Code](https://code.visualstudio.com)
- Add `shell command` to use `code` command
- Toggle console shortcut: `{"key": "alt+cmd+i","command": "workbench.action.terminal.toggleTerminal"}`

## Git

- [Sourcetree](https://www.sourcetreeapp.com/)
- git aliases `git config --global alias.co checkout && git config --global alias.br branch && git config --global alias.ci commit && git config --global alias.st status && git config --global alias.last 'log -1 HEAD' && git config --global alias.ss stash && git config --global alias.ssa 'stash apply'`
- [git-branch-delete](https://github.com/stefanwille/git-branch-delete)

## Utils

- [Amphetamine](https://apps.apple.com/fr/app/amphetamine/id937984704?mt=12)
- [Gifski](https://gif.ski/)
- [HTTPie](https://httpie.io/)
- [Rectangle](https://rectangleapp.com/)
- [Delete Apps Uninstaller](https://apps.apple.com/fr/app/delete-apps-uninstaller/id1033808943?mt=12)
- [Flux](https://justgetflux.com/)

## Luncher

- [Raycast](https://www.raycast.com/)

### Raycast extensions

- [Clipboard History](https://www.raycast.com/extensions/clipboard-history)
- [Color Picker](https://www.raycast.com/thomas/color-picker)
- [Ollama AI](https://www.raycast.com/massimiliano_pasquini/raycast-ollama)
