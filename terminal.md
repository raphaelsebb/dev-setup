# Terminal

## Install

- Install [brew](https://brew.sh/index_fr) The missing package manager for macOS (or Linux)
- Install [warp terminal](https://www.warp.dev/) Warp is a modern, Rust-based terminal with AI built in so you and your team can build great software, faster.
- Install [nvm](https://github.com/nvm-sh/nvm) Node Version Manager - POSIX-compliant bash script to manage multiple active node.js versions

## Customize

In `~/.zshenv` add `pcd` and `ppcd` function to quickly jump into project folder

```bash
# Usage: pcd [directory]
# Move to given repository in your Projects folder
function pcd() {
    # TODO: set path to your Projects folder
    cd /Users/<username>/Projects/$1
}

function ppcd() {
    # TODO: set path to your personal Projects folder
    cd /Users/<username>/Perso/Projects/$1
}
```
