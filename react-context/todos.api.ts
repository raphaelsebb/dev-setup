import type { ListTodosParams, Todo, TodoResponse } from './todos.types'

export class TodosApi {
  private static apiUrl = 'https://jsonplaceholder.typicode.com'

  private static format = (todoResponse: TodoResponse): Todo => {
    return {
      id: todoResponse.id.toString(),
      title: todoResponse.title,
      isDone: todoResponse.completed,
    }
  }

  public static async listTodos({ userId }: ListTodosParams): Promise<Todo[]> {
    const response = await fetch(`${this.apiUrl}/users/${userId}/todos`)
    const todosResponse = await response.json()
    return todosResponse.map((todo: TodoResponse) => this.format(todo))
  }
}
