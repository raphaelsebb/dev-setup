import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react'

import type { FC, PropsWithChildren } from 'react'
import { TodosApi } from './todos.api'
import type { Todo } from './todos.types'

interface TodosContextProps {
  // Define the type of the props here
  userId: string
}

interface TodosContextType {
  // Define the type of the context here
  todos: Todo[]
  isLoaded: boolean
  isLoading: boolean
  refresh: () => void
  add: (todo: Todo) => void
  remove: (todoId: string) => void
  markAsDone: (todoId: string) => void
  update: (todoId: string, data: Todo) => void
}

const TodosContext = createContext<TodosContextType | undefined>(undefined)

export const TodosProvider: FC<PropsWithChildren<TodosContextProps>> = ({
  userId,
  children,
}) => {
  const [isLoaded, setIsLoaded] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [todos, setTodos] = useState<Todo[]>([])

  const refresh = useCallback(async () => {
    setIsLoading(true)
    const todos = await TodosApi.listTodos({ userId })
    setTodos(todos)
    setIsLoading(false)
  }, [userId])

  // Initial load
  useEffect(() => {
    if (isLoaded) {
      return
    }
    refresh().then(() => setIsLoaded(true))
  }, [isLoaded, refresh])

  const add = (todo: Todo) => {
    setTodos([...todos, todo])
  }

  const remove = (todoId: string) => {
    setTodos(todos.filter((todo) => todo.id !== todoId))
  }

  const update = (todoId: string, data: Todo) => {
    setTodos(
      todos.map((todo) => {
        if (todo.id === todoId) {
          return data
        }
        return todo
      })
    )
  }

  const markAsDone = (todoId: string) => {
    const todo = todos.find((todo) => todo.id === todoId)
    if (!todo) {
      return
    }
    update(todoId, { ...todo, isDone: true })
  }

  return (
    <TodosContext.Provider
      value={{
        todos,
        isLoaded,
        isLoading,
        refresh,
        add,
        remove,
        markAsDone,
        update,
      }}
    >
      {children}
    </TodosContext.Provider>
  )
}

export const useTodos = () => {
  const todosContext = useContext(TodosContext)

  if (!todosContext) {
    throw new Error('useTodos has to be used within <TodosContext.Provider>')
  }

  return todosContext
}
