export interface Todo {
  id: string
  title: string
  isDone: boolean
  message?: string
  action?: () => void
}

export interface TodoResponse {
  userId: number
  id: number
  title: string
  completed: boolean
}

export interface ListTodosParams {
  userId: string
}

export interface GetTodoParams {
  userId: string
  todoId: string
}
